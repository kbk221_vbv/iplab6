let sliderBox = document.querySelector('.slider-box');
let images = document.querySelector('.images-list').children; // array of images nodes
let imagesList = document.querySelector('.images-list'); 
let offset = 0; // offset from left border
let currentImage = 0; // current image


window.addEventListener('load', () => {
    setSliderBoxSize();
});

window.addEventListener('click', event => {
    let clickedZone = event.target;
    let previousButton = document.querySelector('.previous-button');
    let nextButton = document.querySelector('.next-button');

    if (clickedZone == nextButton) {
        if (currentImage < (images.length - 1)) {
            nextImage();
        }
    } else if (clickedZone == previousButton) {
        if (currentImage > 0) {
            previousImage();
        }
    }
});

function setSliderBoxSize() {
    let imageStyle = getComputedStyle(images[currentImage]);
    sliderBox.style.width = imageStyle.width;
}

function nextImage() {
    currentImage += 1;
    setSliderBoxSize();

    let previousImageWidth = parseFloat(getComputedStyle(images[currentImage-1]).width);
    offset = offset - (previousImageWidth + 200);
    imagesList.style.left = offset + 'px';
}

function previousImage() {
    currentImage -= 1;
    setSliderBoxSize();

    let nextImageWidth = parseFloat(getComputedStyle(images[currentImage]).width);
    offset = offset + (nextImageWidth + 200);  
    imagesList.style.left = offset + 'px'; 
}

