let firstTaskInput = document.querySelector("#first-task-input");

firstTaskInput.oninput = () => {
    console.log(firstTaskInput.value)
    for (let i = 0; i < 10; i++) {
        if (firstTaskInput.value.includes(i)) {
            let newString = firstTaskInput.value.replace(i, '');
            firstTaskInput.value = newString;
        }
    }
}

let footballField = document.querySelector(".football-field-div");
let footballBall = document.querySelector("#football-ball");
let mouseClickX, mouseClickY, squareStartX, squareStartY;

squareStartX = parseInt(getComputedStyle(footballBall).left);
squareStartY = parseInt(getComputedStyle(footballBall).top);
footballBall.style.top = squareStartY + "px";
footballBall.style.left = squareStartX + "px";

footballField.addEventListener('click', event => {
    mouseClickX = event.pageX;
    mouseClickY = event.pageY;
    footballBall.style.top = mouseClickY - 20 + "px";
    footballBall.style.left = mouseClickX - 20 + "px";
});

// Third task
let flag = false;

document.onkeydown = function (event) {
    if (event.code == "ControlLeft") {
        event.preventDefault()
        flag = true;
    }
    if (event.code == "KeyD" && flag) {
        event.preventDefault()
        console.log("ctrl+d")
        flag = false;
        deleteButtonClick(document.querySelector(".delete-button"))
    }
    if (event.code == "KeyR" && flag) {
        event.preventDefault()
        console.log("ctrl+r")
        flag = false;
        correctButtonClick(document.querySelector(".correct-button"))
    }
}

document.querySelector("#third-task-area").addEventListener('click', clickArea);

function clickArea(event) {
    let deleteButton = document.querySelectorAll('.delete-button');
    let correctButton = document.querySelectorAll('.correct-button');
    let clickedArea = event.target;
    for (let i = 0; i < deleteButton.length; i++) {
        if (deleteButton[i] == clickedArea) {
            deleteButtonClick(clickedArea);
            return null;
        } else if (correctButton[i] == clickedArea) {
            correctButtonClick(clickedArea);
            return null;
        }
    }
    otherClick(clickedArea);
}

function correctButtonClick(button) {
    let textArea = button.parentNode.lastElementChild;
    textArea.toggleAttribute('disabled');
    textArea.classList.toggle('textarea-active');
    button.classList.toggle('active-button');
}

function deleteButtonClick(button) {
    let parentNode = button.parentNode;
    let textArea = parentNode.lastElementChild;
    textArea.value = null;
}

function otherClick(clickedArea) {
    let allTextAreas = document.querySelectorAll('textarea');
    for (let i = 0; i < allTextAreas.length; i++) {
        if (clickedArea != allTextAreas[i]) {
            allTextAreas[i].setAttribute('disabled', 'disabled');
            allTextAreas[i].classList.remove('textarea-active');
            let correctButton = allTextAreas[i].parentNode.children[1];
            correctButton.classList.remove('active-button');
        }
    }
}

// Fifth task

document.querySelector(".fifth-task-table").addEventListener("click", calculatorHandler);
let calculatorResult = document.querySelector("#calculator-result");

let calculatorString = "";
let result = 0;

function calculatorHandler(event) {
    if (event.target.innerText != "=") {
        if (event.target != calculatorResult) {
            calculatorString += event.target.innerText;
            calculatorResult.innerText = calculatorString;
        }
    } else {
        result = eval(calculatorString);
        calculatorResult.innerText = result;
        calculatorString = result;
    }
}